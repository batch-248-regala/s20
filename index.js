/*
(Instructions)
Create a loop that will print out a number if it's divisible by 5 based on a number provided by the user

If the number is less than 50, the loop will stop

If the number is divisible by 10, the number will not be printed and will be skipped

Create another loop that will take in a string and store all the consonants into another variable removing all vowels from it.
*/

/*
	Loop to print out a number if it's divisible by 5
	let userNumber = Number(prompt("Enter a number:"));

	console.log("The number provided is: " + userNumber);

	while (userNumber <= 50) {
	if (userNumber % 10 === 0) {
	console.log("The number is divisible by 10. Skipping the number.");
	} else if (userNumber % 5 === 0) {
	console.log(userNumber);
	} else if (userNumber === 50) {
	console.log("The current value is at 50. Terminating the loop.");
	break;
	}
	userNumber++;
	}

	let userString = prompt("Enter a string:");
	let consonants = "";

	for (let i = 0; i < userString.length; i++) {
	let currentChar = userString[i].toLowerCase();
	if (currentChar !== "a" && currentChar !== "e" && currentChar !== "i" && currentChar !== "o" && currentChar !== "u") {
	consonants += currentChar;
	}
	}

	console.log("The string with all vowels removed is: " + consonants);
/*
(Detailed Instructions)
Create a variable number that will store the value of the number provided by the user via the prompt.

Log the number and print "The number provided is"

Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.

Create a condition that if the current value is less than or equal to 50, stop the loop.

Create another condition that if the current value is divisible by 10, print "The number is divisible by 10. Skipping the number" continue to the next iteration of the loop. 

If the current value is 50, print "The current value is at 50. Terminating the loop."

Create another condition that if the current value is divisible by 5, print the number.

Create a variable that will contain the string supercalifragilisticexpialidocious.

Create another variable that will store the consonants from the string.

Create another for Loop that will iterate through the individual letters of the string based on it's length.

Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.

Create an else statement that will add the letter to the second variable.
*/

/* 
------------TRIAL-------------TRIAL-------------TRIAL-------------TRIAL-------------TRIAL-------------
	//to store the number provided by the user
	let number = Number(prompt("Enter a number:"));

	console.log("The number provided is: " + number);

	// for the loop to iterate through the numbers starting from the user input, down to 0
	for (let i = number; i >= 0; i--) {
	  // to check if the current value is less than or equal to 50
	  if (i <= 50) {
	    break;
	  }
	  // to check if the current value is divisible by 10
	  if (i % 10 === 0) {
	    console.log("The number is divisible by 10. Skipping the number.");
	    continue;
	  }
	  // display if at 50
	  if (i === 50) {
	    console.log("The current value is at 50. Terminating the loop.");
	    break;
	  // to check if the current value is divisible by 5
	  if (i % 5 === 0) {
	    console.log(i);
	  }
	}

	// to store the string
	let string = "supercalifragilisticexpialidocious";
	let consonantString = "";

	// for the loop to iterate through the individual letters of the string
	for (let i = 0; i < string.length; i++) {
	  // to check if the letter is a vowel
	  if (string[i].toLowerCase() === "a" || string[i].toLowerCase() === "e" || string[i].toLowerCase() === "i" || string[i].toLowerCase() === "o" || string[i].toLowerCase() === "u") {
	    // tells the code to continue the next iteration of the loop
	    continue;
	  }
	  // Add the letter to the consonantString variable if it's not a vowel
	  else {
	    consonantString += string[i]; //consonantString = consonantString + string[i]
	    	// to add the character located at the ith index of the string to the consonantString variable
	    	// += means that the value of consonantString is being increased by the value of string[i]

	 	// for each iteration of the loop, if the character of the string at the ith index is not a vowel, it will be added to the consonantString variable
		}
	}

	console.log("The string with all vowels removed is: " + consonantString);
	}
*/

// Store the user-provided number
let number = Number(prompt("Enter a number:"));
console.log(`The number provided is ${number}`);

// Loop through the numbers
for (let i = number; i >= 0; i--) {
  // Display if at 50
  if (i === 50) {
    console.log("The current value is at 50. Terminating the loop.");
    break;	
  }  
  // Check if the number is less than or equal to 50
  if (i <= 50) {
    break;
  }
  // Check if the number is divisible by 10
  else if (i % 10 === 0) {
    console.log("The number is divisible by 10. Skipping the number.");
    continue;
  }
  // Check if the number is divisible by 5
  else if (i % 5 === 0) {
    console.log(i);
  }
}

// Store the string
let string = "supercalifragilisticexpialidocious";
let consonants = "";

// Loop through the string
for (let i = 0; i < string.length; i++) {
  let letter = string[i].toLowerCase();
  // Check if the letter is a vowel
  if (letter === "a" || letter === "e" || letter === "i" || letter === "o" || letter === "u") {
    continue;
  }
  // Add the letter to the consonants
  consonants += letter;
}
console.log(`Stored in the string is "supercalifragilisticexpialidocious"`);
console.log(`The string with all vowels removed is: ${consonants}`);





